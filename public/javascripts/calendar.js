import '../sass/style.scss';
import axios from 'axios';
import moment from 'moment';
import { Calendar } from '@fullcalendar/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import listPlugin from '@fullcalendar/list';
import Tooltip from 'tooltip.js';

const util = {
	// Read cookie value
	readCookie: function(key) {
		const cookie = decodeURIComponent(document.cookie);
		const value = cookie.split(key+'=').pop();
		return value;
	},
	// Convert ms to hh:mm (for seconds, pass true as second arg)
	msToTime: function(duration, showSeconds) {
		var seconds = parseInt((duration/1000)%60);
		var minutes = parseInt((duration/(1000*60))%60);
		var hours = parseInt((duration/(1000*60*60))%24);
		minutes = (minutes < 10) ? "0" + minutes : minutes;
		seconds = (seconds < 10) ? "0" + seconds : seconds;
		if (duration) {
			return showSeconds ? 
				hours + ":" + minutes + ":" + seconds : 
				hours + ":" + minutes;
		}
	},
	flashMessage: function(category, message) {
		const markup = `<div class="alert alert-${category}"><span>${message}</span></div>`
		$('.content').append(markup);
		setTimeout(function() {
			$('.alert').fadeOut();
		}, 3000)
	}
}

if (window.location.pathname.length < 10) {
	function renderCalendar() {
		var calendarEl = document.getElementById('calendar');
		var calendar = new Calendar(calendarEl, {
			plugins: [ dayGridPlugin, listPlugin ],
			defaultView: 'dayGridMonth',
			eventTextColor: '#fff',
			eventTimeFormat: {
				hour: 'numeric',
				minute: 'numeric',
				omitZeroMinute: true,
				meridiem: 'short'
			},
			header: {
				right:  'dayGridMonth,listMonth today prev,next'
			},
			eventRender: function(info) {
				const title = info.event.title;
				const filled = info.event.extendedProps.filledSlots;
				const slots = info.event.extendedProps.availableSlots;
				const id = info.event.extendedProps._id;
				var tooltip = new Tooltip(info.el, {
					title: `${title}<br>
						Volunteers: ${filled}/${slots}
						<span class="hidden eventId">${id}</span>`,
					placement: 'top',
					trigger: 'hover',
					html: true,
					container: '#calendar'
				});
			},
			eventClick: function(info) {
				const id = info.event.extendedProps._id;
				window.location = `/calendar/${id}`;
			}
		});
		axios
			.get('/api/calendar/getEvents')
			.then(res => {
				for (var i = 0; i < res.data.length; i++) {
					switch(res.data[i].category) {
						case 'Concession Stand':
							res.data[i].backgroundColor = '#C23B23';
							break;
						case 'Marching Band':
							res.data[i].backgroundColor = '#579ABE';
							break;
						case 'Percussion Ensemble':
							res.data[i].backgroundColor = '#03C03C';
							break;
						case 'Band':
							res.data[i].backgroundColor = '#EADA52';
							break;
						default:
							res.data[i].backgroundColor = '#F39A27';
					}
					calendar.addEvent(res.data[i]);
				}
			})
			.catch(error => {
				// console.log(error);
			});
		calendar.render();
	}
	renderCalendar();
}
