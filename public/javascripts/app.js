import '../sass/style.scss';
import axios from 'axios';
import moment from 'moment';

const util = {
	// Enable bootstrap popovers
	initPopover: function() {
		$('[data-toggle="popover"]').popover()
	},
	// Read cookie value
	readCookie: function(key) {
		const cookie = decodeURIComponent(document.cookie);
		const value = cookie.split(key+'=').pop();
		return value;
	},
	// Convert ms to hh:mm (for seconds, pass true as second arg)
	msToTime: function(duration, showSeconds) {
		var seconds = parseInt((duration/1000)%60);
		var minutes = parseInt((duration/(1000*60))%60);
		var hours = parseInt((duration/(1000*60*60))%24);
		minutes = (minutes < 10) ? "0" + minutes : minutes;
		seconds = (seconds < 10) ? "0" + seconds : seconds;
		if (duration) {
			return showSeconds ? 
				hours + ":" + minutes + ":" + seconds : 
				hours + ":" + minutes;
		}
	},
	flashMessage: function(category, message) {
		const markup = `<div class="alert alert-${category}"><span>${message}</span></div>`
		$('.content').append(markup);
		setTimeout(function() {
			$('.alert').fadeOut();
		}, 3000)
	}
}
util.initPopover();

// Start Shift
if (window.location.pathname.indexOf('time-tracker') > 0) {
	function startShift() {
		$('.btn-start').click(function() {
			const startTime = moment().format('YYYY-MM-DD HH:mm');
			$('input#startTime').val(startTime);
			$('form#startShift').submit();
			const name = $('#name').val();
			const student = $('#student :selected').text();
			axios
				.post(`/api/email/shift-notification`, {'subject': `Shift started: ${name}`, name, student, startTime})
				.then(res => {
					console.log(res.data);
				})
				.catch(error => {
					console.log(error);
				})
		});
	};
	startShift();
}

// App - Progress
const progress = {
	max_hours: (10 * 3600000),
	init: function() {
	// Initialize each card
		$('.progress-card').each(function() {
			const card = $(this);
			const name = card.find('.shiftName').text();
			const startTime = moment(card.find('.start-time').val());
			const student = card.find('.student').val();
			if (card.hasClass('active')) {
				progress.updateProgress(card, startTime, student, name), 1000
			}

			card.find('.end-shift').one('click', function() {
				const id = $(this).attr("data-id");
				progress.endShift(id, startTime, student, name);
			});
		});

	// Button to clear a shift card
		$('.remove-card').click(function() {
			const id = $(this).attr('data-id');
			progress.clearShift(id);
			$(`#${id}`).parent().remove();
		});

	// Fill in the details for Email Details Modal
		$('.email-button').click(function() {
			const id = $(this).attr('data-id');
			progress.emailModal(id);
		})

		$('.btn.email').click(function() {
			console.log('email submit clicked');
		})
	},
	updateProgress: function(card, startTime, student, name) {
		const updater = setInterval(function() {
			const max_hours = progress.max_hours;
			const now = moment();
			const elapsed = now.diff(startTime);
			const remaining = startTime.diff(now-max_hours) + 1000;
			const percent = ((elapsed/max_hours)*100).toFixed(2);
			card.find('.progress-bar').width(percent+'%').text(percent+'%').attr('aria-valuenow', percent);
			card.find('.elapsed').text(util.msToTime(elapsed, true));
			card.find('.remaining').text(util.msToTime(remaining, true));
			if(elapsed > max_hours) {
				const id = card.find('.end-shift').attr('data-id');
				progress.endShift(id, startTime, student, name);
				clearInterval(updater);
			}
		}, 1000);
	},
	endShift: function(id, startTime, student, name) {
		const max_hours = progress.max_hours;
		var endTime = moment().format('YYYY-MM-DD HH:mm');
		var duration = moment(endTime).diff(startTime);


		// if cap duration to max hours 
		if(duration > max_hours) {
			console.log("Duration > Max Hours");
			duration = max_hours;
			endTime = moment(startTime).add(duration).format('YYYY-MM-DD HH:mm');
		}


		// Update shift - save end time
		axios
			.put(`/api/shift/${id}/end`, {
				'endTime': endTime,
				'duration': duration
			})
			.then(res => {
				const id = res.data._id;
				const duration = util.msToTime(res.data.duration, true);
				util.flashMessage('success',`${res.data.name} has been signed out at ${moment(res.data.endTime).format('hh:mm')}. Shift duration: ${duration}`);
				$(`#${id}`).addClass('ended');
				$(`#${id}`).find('.row').toggleClass('d-none');
				$(`#${id}`).find('.duration').text(`${duration}`);
			})
			.catch(error => {
				console.log(error);
			});

		// send email notification to admin
		startTime = startTime.format('YYYY-MM-DD HH:mm');
		axios
			.post(`/api/email/shift-notification`, {'subject': `Shift ended: ${name}`, name, student, startTime, endTime, duration})
			.then(res => {
				console.log(res.data);
			})
			.catch(error => {
				console.log(error);
			})
	},
	emailModal: function(id) {
		axios
			.get(`/api/shift/${id}`)
			.then(res => {
				const modal = $('#email-details');
				const ended = moment(res.data.endTime).format('M/D h:mm a');
				const name = res.data.name;
				const duration = util.msToTime(res.data.duration);
				modal.find('.name').text(name).val(name);
				modal.find('.ended').text(ended).val(ended);
				modal.find('.duration').text(duration).val(duration);
			})
	},
	clearShift: function(id) {
		axios
			.put(`/api/shift/${id}/clear`)
			.then(res => {
				console.log('card removed');
				console.log(res.body);
			})
			.catch(error => {
				console.log(error);
			})
	}
}


if (window.location.pathname.indexOf('progress') > 0) {
	progress.init();
}