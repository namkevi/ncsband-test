import '../sass/style.scss';
import axios from 'axios';
import moment from 'moment';
import { Calendar } from '@fullcalendar/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import listPlugin from '@fullcalendar/list';
import Tooltip from 'tooltip.js';

// Global
const util = {
	// Enable bootstrap popovers
	init: function() {
		if(window.location.href.indexOf('admin')) {
			$('input[name="email"]').focus();
		}
	},
	// Read cookie value
	readCookie: function(key) {
		const cookie = decodeURIComponent(document.cookie);
		const value = cookie.split(key+'=').pop();
		return value;
	},
	// Convert ms to hh:mm (for seconds, pass true as second arg)
	msToTime: function(duration, showSeconds) {
		var seconds = parseInt((duration/1000)%60);
		var minutes = parseInt((duration/(1000*60))%60);
		var hours = parseInt((duration/(1000*60*60))%24);
		minutes = (minutes < 10) ? "0" + minutes : minutes;
		seconds = (seconds < 10) ? "0" + seconds : seconds;
		if (duration) {
			return showSeconds ? 
				hours + ":" + minutes + ":" + seconds : 
				hours + ":" + minutes;
		}
	},
	timeToMs: function(time) {
	    const hour = time.split(':')[0];
	    const minute = time.split(':')[1];
	    return (hour*1000*60*60 + minute*1000*60);
	},
	flashMessage: function(category, message) {
		const markup = `<div class="alert alert-${category}"><span>${message}</span></div>`
		$('.content').append(markup);
		setTimeout(function() {
			$('.alert').fadeOut();
		}, 3000)
	}
}
util.init();

const exportData = {
	init: function() {
		$('#exportCSV').click(function() {
			const model = $(this).attr('data-model');
			exportData.exportCSV(model);
		});
		$('#testCSV').click(function() {
			exportData.testCSV();
		});
	},
	exportCSV: function(model) {
		axios
			.get(`/api/downloads/${model}`)
			.then(res => {
				console.log('success');
				window.open(`/exports/${res.data}`);
			})
			.catch(error => {
				console.log(error);
				util.flashMessage('danger', 'Something failed');
			});
	}
}
exportData.init();

// Admin - Manage Shifts
const shifts = {
	init: function() {
		$('.delete-shift').click(function() {
			let really = confirm("Are you sure you want to delete this shift? It will affect the student's total earnings");
			if (really) {
				const id = $(this).attr('data-id');
				shifts.deleteShift(id);
			}
		});
		$('.edit-shift').click(function() {
			const id = $(this).attr('data-id');
			$('#edit-shift-modal .hidden-id').val(id);
			shifts.populateShift(id);
		});
		// Add shift: update duration when start/end times are changed
		$('#add-shift-modal input[name="endTime"], #add-shift-modal input[name="startTime"]')
		.on('blur change',function() {
			const date = $('#add-shift-modal input[name="date"').val(),
				  endTime = moment(date + ' ' + $('#add-shift-modal input[name="endTime"]').val()),
				  startTime = moment(date + ' ' + $('#add-shift-modal input[name="startTime"]').val()),
				  duration = endTime.diff(startTime);
			$('#add-shift-modal .duration').val(util.msToTime(duration));
		});
		// Add shift: update end time when duration is changed
		$('#add-shift-modal input.duration').on('blur change', function() {
			const date = $('#add-shift-modal input[name="date"').val(),
				  startTime = moment(date + ' ' + $('#add-shift-modal input[name="startTime"]').val()),
				  duration = util.timeToMs($(this).val()),
				  endTime = startTime.add(duration).format('HH:mm');
			$('#add-shift-modal input[name="endTime"]').val(endTime);
		});
		// Edit shift: update duration when start/end times are changed
		$('#edit-shift-modal input[name="endTime"], #edit-shift-modal input[name="startTime"]')
		.on('blur change',function() {
			const date = $('#edit-shift-modal input[name="date"').val(),
				  endTime = moment(date + ' ' + $('#edit-shift-modal input[name="endTime"]').val()),
				  startTime = moment(date + ' ' + $('#edit-shift-modal input[name="startTime"]').val()),
				  duration = endTime.diff(startTime);
			$('#edit-shift-modal .duration').val(util.msToTime(duration));
		});
		// Edit shift: update end time when duration is changed
		$('#edit-shift-modal input.duration').on('blur change', function() {
			const date = $('#edit-shift-modal input[name="date"').val(),
				  startTime = moment(date + ' ' + $('#edit-shift-modal input[name="startTime"]').val()),
				  duration = util.timeToMs($(this).val()),
				  endTime = startTime.add(duration).format('HH:mm');
			$('#edit-shift-modal input[name="endTime"]').val(endTime);
		});
		$('.clear-shifts').click(function() {
			if (confirm('Are you sure? This will delete all shift records!')) {
				shifts.clearShifts();
			}
		});
		// Apply sort parameters
		$('#applySort').click(function() {
			shifts.sortParams($(this).attr('data-page'));
		});
		$('.paginate').click(function() {
			shifts.sortParams($(this).attr('data-page'));
		})
	},
	sortParams: function(page) {
		const sortby = $('#sortby').val();
		const order = $('input[name="order"]').val();
		const limit = $('#limit').val();
		const query = `?sort=${sortby}&order=${order}&limit=${limit}&page=${page}`;
		console.log(query);
		window.location.href = '/admin/shifts' + query;
	},
	populateShift: function(id) {
		axios
			.get(`/admin/api/shift/${id}`)
			.then(res => {
				const duration = moment(res.data.endTime).diff(moment(res.data.startTime));
				console.log('diff '+duration);
				const date = res.data.startTime.split(/[T\s]/)[0];
				const startTime = res.data.startTime.split(/[T\s]/)[1].split(/[\+\-]/)[0];
				const endTime = res.data.endTime.split(/[T\s]/)[1].split(/[\+\-]/)[0];
				$('#edit-shift-modal input[name="name"]').val(res.data.name);
				$('#edit-shift-modal select[name="student"]').val(res.data.student);
				$('#edit-shift-modal input[name="date"]').val(date);
				$('#edit-shift-modal input[name="startTime"]').val(startTime);
				$('#edit-shift-modal input[name="endTime"]').val(endTime);
				$('#edit-shift-modal textarea[name="notes"]').val(res.data.notes);
				if(!res.data.duration) {
					$('#edit-shift-modal .duration').val(util.msToTime(duration, true));
				} else {
					$('#edit-shift-modal .duration').val(util.msToTime(res.data.duration, true));
				}
			})
			.catch(error => {
				console.log(error);
				util.flashMessage('danger', 'Something failed');
			});
	},
	deleteShift: function(id) {
		axios
			.delete(`/api/shift/${id}`)
			.then(res => {
				util.flashMessage('success', 'Shift removed.');
				$(`tr.${id}`).remove();
			})
			.catch(error => {
				console.log(error);
				util.flashMessage('danger', 'Something failed');
			});
	},
	clearShifts: function() {
		axios
			.delete('/api/shift/clear')
			.then(res => {
				location.reload();
			})
			.catch(error => {
				console.log(error);
				util.flashMessage('danger', 'Something failed');
			});
	}
}
if (window.location.pathname.indexOf('shifts') > -1) {
	shifts.init();
}

// Admin - Students 
const students = {
	init: function() {
		$('.delete-student').click(function() {
			const id = $(this).attr('data-id');
			students.deleteStudent(id);
		});
		$('.reset-earnings').click(function() {
			if (confirm('Are you sure? This will delete all shift entries for this student!')) {
				const id= $(this).attr('data-id');
				students.resetEarnings(id);
			}
		});
		$('.reset-all').click(function() {
			if (confirm('Are you sure? This will delete all shift entries for all students!')) {
				students.resetAllEarnings();
			}
		})
	},
	deleteStudent: function(id) {
		axios
			.delete(`/api/student/${id}/delete`)
			.then(res => {
				util.flashMessage('success', 'Student deleted');
				$(`tr.${id}`).remove();
			})
			.catch(error => {
				console.log(error);
				util.flashMessage('danger', 'Something failed');
			});
	},
	resetEarnings: function(id) {
		axios
			.put(`/api/student/reset`, {'id': id})
			.then(res => {
				util.flashMessage('success', 'Earnings reset');
				$(`tr.${id} .earnings`).text('$0.00');
				$(`tr.${id} .hours`).text('0');
			})
			.catch(error => {
				console.log(error);
				util.flashMessage('danger', 'Something failed');
			});
	},
	resetAllEarnings: function() {
		axios
			.put('/api/student/reset-all')
			.then(res => {
				util.flashMessage('success', 'All earnings reset');
				$('.earnings').text('$0.00');
				$('.hours').text('0');
			})
			.catch(error => {
				console.log(error);
				util.flashMessage('danger', 'Something failed');
			});
	}
}
if (window.location.pathname.indexOf('students') > -1) {
	students.init();
}

const users = {
	init: function() {
		'/api/user/:id/delete'
		$('.delete-user').click(function() {
			let really = confirm("Are you sure you want to delete this user?");
			if (really) {
				const id = $(this).attr('data-id');
				users.deleteUser(id);
			}
		});
	},
	deleteUser: function(id) {
		axios
			.delete(`/api/user/${id}/delete`)
			.then(res => {
				util.flashMessage('success', 'User deleted');
				$(`tr.${id}`).remove();
			})
			.catch(error => {
				console.log(error);
				util.flashMessage('danger', 'Something failed');
			});
	}
}
if (window.location.pathname.indexOf('users') > -1) {
	users.init();
}

const calendar = {
	init: function() {
		var calendarEl = document.getElementById('calendar');
		var calendarInstance = new Calendar(calendarEl, {
			plugins: [ dayGridPlugin, listPlugin ],
			defaultView: 'dayGridMonth',
			eventTextColor: '#fff',
			eventTimeFormat: {
				hour: 'numeric',
				minute: 'numeric',
				omitZeroMinute: true,
				meridiem: 'short'
			},
			header: {
				right:  'dayGridMonth,listMonth today prev,next'
			},
			eventRender: function(info) {
				const title = info.event.title;
				const filled = info.event.extendedProps.filledSlots;
				const slots = info.event.extendedProps.availableSlots;
				const id = info.event.extendedProps._id;
				const tip = `
					${title}<br>Volunteers: ${filled}/${slots}
					<span class="hidden">${id}</span>
					<button class="btn btn-danger btn-block btn-sm deleteEvent" data-event-id="${id}">Delete Event</button>
					<button class="btn btn-info btn-block btn-sm copyEvent" data-toggle="modal" data-target="#add-event-modal" data-event-id="${id}">Copy Event</button>
					<a href="/admin/calendar/${id}" class="btn btn-primary btn-block btn-sm viewEvent">View Event</a>
					`
				var tooltip = new Tooltip(info.el, {
					title: tip,
					placement: 'top',
					trigger: 'click',
					closeOnClickOutside: true,
					html: true,
					container: '#calendar'
				});
				tooltip.show();
				tooltip.hide();

			}, // end: eventRender
			eventClick: function(info) {
				const id = info.event.extendedProps._id;
				$('.copyEvent').click(function() {
					$('.tooltip').css('visibility','hidden');
					calendar.copyEvent(id);
				});
				$('.deleteEvent').click(function() {
					var deleteConfirm = confirm('Delete this event?');
					if (deleteConfirm) {
						calendar.deleteEvent(id);
						$('.tooltip').css('visibility','hidden');
					}
				});
			}

		}); // end: calendarInstance

		// get events from database and add to calendar object
		axios
			.get('/api/calendar/getEvents')
			.then(res => {
				for (var i = 0; i < res.data.length; i++) {
					switch(res.data[i].category) {
						case 'Concession Stand':
							res.data[i].backgroundColor = '#C23B23';
							break;
						case 'Marching Band':
							res.data[i].backgroundColor = '#579ABE';
							break;
						case 'Percussion Ensemble':
							res.data[i].backgroundColor = '#03C03C';
							break;
						case 'Band':
							res.data[i].backgroundColor = '#EADA52';
							break;
						default:
							res.data[i].backgroundColor = '#F39A27';
					}
					calendarInstance.addEvent(res.data[i]);
				}
			})
			.catch(error => {
				console.log(error);
			});

		// render calendar
		calendarInstance.render();

		// Show/hide multi-date fields
		$('#multiDate').change(function() {
			if(this.checked) {
				$('.multidate').removeClass('d-none');
				const defaultTime = $('input[name="startTime"]').val();
				$('input[name="time_1"]').val(defaultTime);
			} else {
				$('.multidate').addClass('d-none');
			}
		});

		$('#add-date').click(function() {
			let count = parseInt($(this).attr('data-count')) + 1;
			$(this).attr('data-count', count);
			$('input[name="count"]').attr('value', count);
			console.log($(this).attr('data-count'));

			let inputs = `
				<div class="form-group form-row multidate-${count}">
					<div class="col-4">
						<input type="date" name="date_${count}" required class="form-control">
					</div>
					<div class="col-4">
						<input type="time" name="time_${count}" required class="form-control">
					</div>
					<div class="col-4">
						<input type="time" name="end_${count}" class="form-control">
					</div>
				</div>`;

			$(this).before(inputs);
			const defaultTime = $('input[name="startTime"]').val();
			$(`input[name="time_${count}"]`).val(defaultTime);
		}) // end: multi-date fields

	}, // end: Init
	copyEvent: function(id) {
		axios
			.get(`/api/calendar/get/${id}`)
			.then(res => {
				const start = res.data.start.split(' ')[0];
				const startTime = res.data.start.split(' ')[1];
				const end = res.data.end.split(' ')[0];
				const endTime = res.data.end.split(' ')[1];
				console.log('start' + start);
				console.log('startTime ' + startTime);
				const modal = $('#add-event-modal');
				modal.find('input[name="title"]').val(res.data.title);
				modal.find('textarea[name="description"]').val(res.data.description);
				modal.find('select[name="category"]').val(res.data.category);
				modal.find('input[name="start"]').val(start).focus();
				modal.find('input[name="startTime"]').val(startTime);
				modal.find('input[name="end"]').val(end);
				modal.find('input[name="endTime"]').val(endTime);
				modal.find('textarea[name="volunteerInformation"]').val(res.data.volunteerInformation);
				modal.find('input[name="availableSlots"]').val(res.data.availableSlots);
			})
			.catch(error => {
				console.log(error);
			});
	},
	deleteEvent: function(id) {
		var title;
		axios
		.delete(`/api/calendar/delete/${id}`)
		.then(res => {
			console.log(res.data);
		})
		.catch(error=> {
			console.log(error);
		});
		util.flashMessage('success', `Event deleted`);
		location.reload();
	}
}
if (window.location.pathname.indexOf('calendar') > -1) {
	calendar.init();
}