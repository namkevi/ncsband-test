const mongoose = require('mongoose');
const User = mongoose.model('User');
const promisify = require('es6-promisify');
const passport = require('passport');

exports.adminLogin = (req, res) => {
	res.render('adminLogin', {'title': 'Admin Login'});
}

exports.adminRegister = (req, res) => {
	res.render('adminRegister', {'title': 'Register Admin'});	
}

exports.validateRegister = (req, res, next) => {
	req.checkBody('email', 'Email not valid').isEmail();
	req.sanitizeBody('email').normalizeEmail();
	req.checkBody('password', 'Password required').notEmpty();
	req.checkBody('password-confirm', 'Confirm Password cannot be blank').notEmpty();
	req.checkBody('password-confirm', 'Passwords do not match').equals(req.body.password);
	const errors = req.validationErrors();
	if (errors) {
		req.flash('danger', errors.map(err => err.msg));
		res.redirect('/back')
		return;
	}
	next();
}

exports.register = async (req, res) => {
	const user = new User({email: req.body.email, access: req.body.access});
	const register = promisify(User.register, User);
	await register(user, req.body.password);
	req.flash('success', 'User created');
	res.redirect('/admin/users');
}

exports.login = passport.authenticate('local', {
	failureRedirect: '/login',
	failureFlash: 'Failed Login! Please try again',
	successRedirect: '/admin/calendar',
	successFlash: 'Logged In'
});

exports.logout = (req, res) => {
	req.logout();
	req.flash('success', 'Logged Out');
	res.redirect('/admin/login');
}

exports.isLoggedIn = (req, res, next) => {
	if(req.isAuthenticated()) {
		console.log(req.user.access);
		next();
		return;
	}
	req.flash('danger', 'Must be logged in to access page');
	res.redirect('/admin/login');
}

exports.isAdmin = (req, res, next) => {
	if(req.user.access == 0) {
		next();
		return;
	}
	req.flash('danger', 'You are not authorized to view this page.');
	res.redirect('back');
}

exports.isManager = (req, res, next) => {
	if(req.user.access == 0 || req.user.access > 1) {
		next();
		return;
	}
	req.flash('danger', 'You are not authorized to view this page.');
	res.redirect('back');
}
