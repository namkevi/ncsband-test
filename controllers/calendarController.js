const mongoose = require('mongoose');
const Variables = mongoose.model('Variables');
const moment = require('moment');
const Event = mongoose.model('Event');
const mail = require('../handlers/mail');

exports.calendarPage = async (req, res) => {
	console.log(req.query.name);
	res.render('calendar', {'title': 'Calendar'});
}
exports.getEvents = async (req, res) => {
	const events = await Event.find();
	res.send(events);
}
exports.eventDetail = async (req, res) => {
	const event = await Event.findOne({_id:req.params.id});
	
	// queries for calendar links
	const fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
	const calStart = moment(event.start).format('YYYYMMDDTHHmmss[Z]');
	const calEnd = moment(event.end).format('YYYYMMDDTHHmmss[Z]') || calStart.add(1, 'hour');
	// google Calendar
	const gEventName = event.title.replace(/[^A-Za-z0-9]/g,'+');
	const gDetails = 'For+details,+link+here:+'+fullUrl;
	const gDates = calStart+'/'+calEnd;
	const gCal = `https://www.google.com/calendar/render?action=TEMPLATE&text=${gEventName}&dates=${gDates}&details=${gDetails}&output=xml`

	const icsMsg = `BEGIN:VCALENDAR\nVERSION:2.0\nBEGIN:VEVENT\nDTSTAMP:${calStart}\nDTSTART:${calStart}\nDTEND:${calEnd}\nSUMMARY:${fullUrl}\nEND:VEVENT\nEND:VCALENDAR`

	res.render('eventDetail', {event, fullUrl, gCal, icsMsg});
}
exports.gcalendarPage = (req, res) => {
	res.render('gcalendar', {'title': 'Calendar'});
}

exports.registerAttendee = async (req, res) => {
	const name = req.body.name;
	const email = req.body.email;
	const note = req.body.note;
	const params = req.params.id;

	const event = await Event.findOneAndUpdate(
		{ _id:req.params.id }, 
		{
			$addToSet: { attendees: {"name": name, "email": email, "note": note} },
			$inc: { filledSlots: 1 }
		},
		{ new: true }
	);

	req.body.event = event;

	const sendMail = await mail.send({
		from: 'info@ncsband.com',
		to: email,
		subject: `Volunteer confirmation`,
		filename: 'email-rsvp',
		fields: req.body
	});
	if(!sendMail) {
		req.flash('error', 'Email failed to send. Sorry!');
		res.redirect('back');
	} else {
		req.flash('success', `Email sent to ${req.body.email}`);
		res.redirect('back');
	}
}

exports.cancelRSVP = async (req, res) => {
	const event = await Event.findOneAndUpdate(
		{ 
			_id:req.params.id, 
			filledSlots: { $gte: 1 } 
		}, 
		{
			$pull: { attendees: {"_id": req.params.attendeeID} },
			$inc: { filledSlots: -1 }
		},
		{ new: true }
	);
	console.log('req.query  =  ' + req.query)
	const sendMail = await mail.send({
		from: 'info@ncsband.com',
		to: 'admin@ncsband.com',
		subject: `Volunteer cancelled: ${req.query.name} for event ${req.query.event}`,
		filename: 'email-canceled',
		fields: req.query
	});
	if(!sendMail) {
		console.log('email failed');
	} else {
		console.log('cancellation notification email sent to admins');
	}
	req.flash('success', 'Canceled RSVP');
	res.redirect(`/calendar/${req.params.id}`);
}