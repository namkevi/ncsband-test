const mongoose = require('mongoose');
const Student = mongoose.model('Student');
const Shift = mongoose.model('Shift');
const Variables = mongoose.model('Variables');
const moment = require('moment');
const mail = require('../handlers/mail');
const wage = 8.00

exports.food = (req, res) => {
	res.redirect('https://docs.google.com/forms/d/e/1FAIpQLScV5l_J6g6CpWD9dtxVdJhh0PTaQTbVCENDDlTd4Tr2tPxywg/viewform');
}

exports.appStart = (req, res) => {
	res.redirect('/time-tracker');
}
exports.login = async (req, res) => {
	const students = await Student.find().sort({ name: 1 });
	res.render('login', {'title': 'Log In', students});
}

exports.createShift = async (req, res) => {
	console.log(req.body.startTime);
	const shift = await (new Shift(req.body)).save();
	res.redirect(`/shift/progress`);
}

exports.progressPage = async (req, res) => {
	const recent = moment().subtract(24, 'hours').format('YYYY-MM-DD HH:mm');
	const activeShifts = await Shift.find({endTime: null});
	const recentShifts = await Shift.find({endTime: {'$gte': new Date(recent)}, cleared: false || null })
	res.render('progressPage', {'title': 'Current Shifts', activeShifts, recentShifts});
}

exports.addStudent = async (req, res) => {
	req.body.earnings = 0.00;
	req.body.reset_date = moment().format('YYYY-MM-DD');
	req.body.name = `${req.body.last_name}, ${req.body.first_name}`
	const student = await (new Student(req.body)).save();
	res.redirect(`/time-tracker`);
}

exports.getShift = async (req, res) => {
	const shift = await Shift.findOne({_id:req.params.id});
	res.send(shift);
}

exports.endShift = async (req, res) => {
	console.log(req.body.endTime + " controller endTime");
	const shift = await Shift.findOneAndUpdate(
		{ _id: req.params.id }, 
		{ $set: {
				'endTime': req.body.endTime, 
				'duration': req.body.duration
			} 
		},
		{ new: true }
	);
	res.send(shift);
}

// Adds time from completed shift to student
// exports.addEarnings = async (req, res) => {
// 	const name = req.body.student
// 	const first_name = name.split(', ')[1];
// 	const last_name = name.split(', ')[0];
// 	const duration = req.body.duration;
// 	const earnings = ((duration/3600000) * wage).toFixed(2)
// 	const student = await Student.findOneAndUpdate(
// 		{ 
// 			'first_name': first_name,
// 			'last_name': last_name 
// 		},
// 		{ $inc: {
// 				'hours': duration,
// 				'earnings': earnings
// 			}
// 		},
// 		{ new: true }
// 	);

// 	res.send(student);
// }

exports.clearShift = async (req, res) => {
	const shift = await Shift.findOneAndUpdate(
		{ _id: req.params.id },
		{ $set: {
				'cleared': true, 
			} 
		},
		{ new: true }
	);
	res.send(shift);
}

exports.shiftNotification = async (req, res) => {
	console.log('shift notification initiated');
	console.log(req.body);
	const email = await mail.send({
		from: 'info@ncsband.com',
		to: 'admin@ncsband.com',
		subject: req.body.subject,
		filename: 'email-shift-notification',
		fields: req.body
	});
	if(!email) {
		console.log('email failed!');
	} else {
		console.log('email succeeded');
	}
	res.send(email);
}

exports.emailShift = async (req, res) => {
	console.log('shift email sent');
	const email = await mail.send({
		from: 'info@ncsband.com',
		to: req.body.email,
		subject: `Shift details for ${req.body.name}`,
		filename: 'email-shift',
		fields: req.body
	});
	if(!email) {
		req.flash('error', 'Email failed to send. Sorry!');
		res.redirect('back');
	} else {
		req.flash('success', `Email sent to ${req.body.email}`);
		res.redirect('back');
	}
}