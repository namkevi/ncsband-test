const mongoose = require('mongoose');
const Student = mongoose.model('Student');
const Shift = mongoose.model('Shift');
const User = mongoose.model('User');
const Variables = mongoose.model('Variables');
const Event = mongoose.model('Event');
const moment = require('moment');
const mail = require('../handlers/mail');
const wage = 8.25
const fs = require('fs');
// const { parse } = require('json2csv');
// const { AsyncParser } = require('json2csv');
// const { parseAsync } = require('json2csv');
const jsonexport = require('jsonexport');

function timeToMs(time) {
    const hour = time.split(':')[0];
    const minute = time.split(':')[1];
    return (hour*1000*60*60 + minute*1000*60);
}


// Manage Shifts                 // -------------------------------------------- //
exports.adminShifts = async (req, res) => {
    const q = {
        sort: req.query.sort || 'startTime',
        order: parseInt(req.query.order) || 1,
        limit: parseInt(req.query.limit) || 20,
        page: parseInt(req.query.page) || 1
    }    
    const shifts = await Shift.find().sort({ [q.sort]: [q.order] }).populate('student_detail').skip((q.limit*q.page)-q.limit).limit(q.limit);
    const shiftCount = await Shift.count();
    const students = await Student.find();
    res.render('adminShifts', { 'title': 'Manage Shifts', shifts, students, q, shiftCount});
}
exports.addShift = async (req, res) => {
    // save new shift
    req.body.startTime = moment(req.body.date + ' ' + req.body.startTime).format('YYYY-MM-DD HH:mm');
    req.body.endTime = moment(req.body.date + ' ' + req.body.endTime).format('YYYY-MM-DD HH:mm');
    req.body.cleared = true;
    req.body.duration = timeToMs(req.body.duration);
    const shift = await (new Shift(req.body)).save();
    req.flash('success', `Shift updated for ${req.body.name}`);
    res.send(shift);
}
exports.getShift = async (req, res) => {
    const shift = await Shift.findOne({_id:req.params.id})
    res.send(shift);
}
exports.editShift = async (req, res) => {
    req.body.startTime = moment(req.body.date + ' ' + req.body.startTime).format('YYYY-MM-DD HH:mm:ss');
    req.body.endTime = moment(req.body.date + ' ' + req.body.endTime).format('YYYY-MM-DD HH:mm:ss');
    req.body.duration = timeToMs(req.body.duration);
    console.log(req.body.id);
    const shift = await Shift.findOneAndUpdate(
        {_id: req.body.id},
        {$set: {
            'name': req.body.name,
            'startTime': req.body.startTime,
            'endTime': req.body.endTime,
            'duration': req.body.duration,
            'student': req.body.student,
            'notes': req.body.notes,
            'cleared': true
            }
        }
    );
    req.flash('success', `New shift added for ${req.body.name}`);
    res.redirect('/admin/shifts');
}
exports.deleteShift = async (req, res) => {
    const shift = await Shift.remove({ _id: req.params.id });
    res.send(shift);
}
exports.clearShifts = async (req, res) => {
    const shift = await Shift.remove({});
    req.flash('success', 'Shift records cleared');
    res.send(shift);
}

// Manage Students               // -------------------------------------------- //
exports.adminStudents = async (req, res) => {
    const students = await Student.find().sort({ name: 1, startTime: 1 }).populate('shifts');
    res.render('adminStudents', { 'title': 'Manage Students', students });
}
exports.addStudent = async (req, res) => {
    req.body.name = `${req.body.last_name}, ${req.body.first_name}`
    const student = await (new Student(req.body)).save();
    req.flash('success', `New student added: ${req.body.name}`);
    res.redirect('/admin/students');
}
exports.deleteStudent = async (req, res) => {
    const student = await Student.remove({ _id: req.params.id });
    res.send(student);
}
exports.editStudent = async (req, res) => {
    const student = await Student.findOneAndUpdate({ _id: req.body.id }, {
        $set: {
            'earnings': req.body.earnings,
            'reset_date': new Date()
        }
    }, { new: true });
    res.send(student);
}
exports.resetEarnings = async (req, res) => {
    const shifts = await Shift.find({'student': req.body.id}).remove();
    const student = await Student.findOneAndUpdate({ _id: req.body.id }, {
        $set: {
            'reset_date': new Date()
        }
    }, { new: true });
    res.send(student);
}
exports.resetAllEarnings = async (req, res) => {
    const students = await Student.updateMany(
        {}, 
        {
            $set: {
                'earnings': 0.00,
                'reset_date': new Date()
            }
        }
    );
    res.send(students);
}

// Manage Users                // -------------------------------------------- //
exports.adminUsers = async (req, res) => {
    const users = await User.find({access: {$gt: 0}});
    res.render('adminUsers', {'title': 'Manage Users', users});
}
exports.deleteUser = async (req, res) => {
    const user = await User.remove({ _id: req.params.id });
    res.send(user);
}

// Calendar                    // -------------------------------------------- //
exports.adminCalendar = async (req, res) => {
    res.render('adminCalendar', {'title': 'Manage Calendar'})    
}

exports.addEvent = async (req, res) => {

    req.body.end = req.body.start + ' ' + req.body.endTime;
    req.body.start += ' ' + req.body.startTime;
    const event = await (new Event(req.body)).save(function(err) {
        if(err) { return console.log(err); } 
        console.log(`Created event: ${req.body.title}`);
        req.flash(`Created event: ${req.body.title}`);
    });

    if(req.body.multiDate) {
        const count = req.body.count;
        for(let c = 1; c <= count; c++) {
            const start = req.body[('date_'+c)] +' '+ req.body[('time_'+c)];
            const end = req.body[('date_'+c)] +' '+ req.body[('end_'+c)];
            const event = await (new Event({
                title: req.body.title,
                description: req.body.description,
                availableSlots: req.body.availableSlots,
                category: req.body.category,
                volunteerInformation: req.body.volunteerInformation,
                start: start,
                end: end
            })).save(function(err) {
                if(err) { return console.log(err); } 
                console.log(`Created event: ${req.body.title}`);
                req.flash(`Created event: ${req.body.title}`);
            });
        }
    }
    res.redirect('/admin/calendar');
}

exports.eventDetail = async (req, res) => {
    const event = await Event.findOne({_id:req.params.id});
    res.render('adminEventDetail', {event});
}

exports.getOneEvent = async (req, res) => {
    const event = await Event.findOne({_id:req.params.id});
    res.send(event);
}

exports.deleteEvent = async (req, res) => {
    const event = await Event.remove({ _id: req.params.id });
    res.send(event);
}

exports.editEvent = async (req, res) => {
    console.log('editEvent');
    req.body.end = req.body.start +' '+ req.body.endTime;
    req.body.start += ' ' + req.body.startTime;
    const event = await Event.findOneAndUpdate(
        {_id:req.params.id},
        { $set: {
                title: req.body.title,
                start: req.body.start,
                end: req.body.end,
                volunteerInformation: req.body.volunteerInformation,
                description: req.body.description,
                filledSlots: req.body.filledSlots,
                availableSlots: req.body.availableSlots
            }
        }, 
        { new: true }
    );
    req.flash('success', 'Edited event');
    res.redirect('/admin/calendar');
}

exports.removeVolunteer = async (req, res) => {
    const event = await Event.findOneAndUpdate(
        { 
            _id:req.params.id, 
            filledSlots: { $gte: 1 } 
        }, 
        {
            $pull: { attendees: {"_id": req.params.attendeeID} },
            $inc: { filledSlots: -1 }
        },
        { new: true }
    );
    req.flash('success', 'Canceled RSVP');
    res.redirect(`/admin/calendar/${req.params.id}`);
}

// Manually import student list
exports.studentImportScript = async (req, res) => {
    const list = [{name:"McKenzie, Jarvis"},
    {name:"Algee, Daniel"},
    {name:"Taylor, George"},
    {name:"Court, Caroline"},
    {name:"Crossett, Ryan"},
    {name:"Williams, Jacob"},
    {name:"Crossno, Hannah"},
    {name:"Ramsey, CJ"},
    {name:"Leslie, Lorelei"},
    {name:"Kamler, Drew"},
    {name:"Kinkade, Alyssa"},
    {name:"Phillips, Elizabeth"},
    {name:"Cooley, John"},
    {name:"Roberts, Chase"},
    {name:"Cain, Reagan"},
    {name:"Raby, Jacob"},
    {name:"Meek, Sarah"},
    {name:"Gammell, Sydney"},
    {name:"Fuselier, Will"},
    {name:"Wing, Tamma"},
    {name:"Wing, Tamma"},
    {name:"Barnes, Ian"},
    {name:"Sheridan, Rachel"},
    {name:"Nam, Kevin"},
    {name:"Leslie, Emma"},
    {name:"Leslie, Emma"},
    {name:"Cooley, John"},
    {name:"Robertson, Tyler"},
    {name:"Quon, Samuel"},
    {name:"Starkrt, Leah"},
    {name:"Lott, Madison"},
    {name:"Frank, Leo"},
    {name:"Sanderson, Laurie"},
    {name:"Walls, Melvin"},
    {name:"Roberts, Mallory"}];
    list.forEach(function(entry) {
        const students = new Student(entry).save();
    })
    res.send('success');
}


exports.addVariables = async (req, res) => {
    const variable = await (new Variables(req.body)).save();
    req.flash('success', req.body);
    res.redirect('back');
}

exports.adminVariables = async (req, res) => {
    // const variables = await Variables.find();
    res.render('adminVariables', { 'title': 'Edit Variables' });
}

exports.exportCSV = async (req, res) => {
    const model = req.params.model
    const fileName = `${model}-${moment(new Date).format("YYYY-MM-DD_HHmmss")}.csv`;
    var data;
    switch(model) {
        case 'Student':
            data = await Student.find({}, '-_id name reset_date').lean();
            break;
        case 'Shift':
            data = await Shift.find({}, '-_id -__v -location -cleared').populate('student_detail', '-_id name').lean();
            break;
    }
    for(const shift of data) {
        delete shift.student;
        shift.duration = ((shift.duration/(1000*60*60))%24).toFixed(2);
    }
    jsonexport(data,function(err, csv){
        if(err) return console.log(err);
        fs.writeFile(`./public/exports/${fileName}`, csv, (err) => {
            if(err) throw err;
        })
    });

    res.send(fileName);
}
