const express = require('express');
const router = express.Router();
const appController = require('../controllers/appController');
const userController = require('../controllers/userController');
const adminController = require('../controllers/adminController');
const calendarController = require('../controllers/calendarController');
const { catchErrors } = require('../handlers/errorHandlers');

// App Routes
router.get('/food', appController.food);

router.get('/app', appController.appStart);
router.get('/time-tracker', catchErrors(appController.login));
router.post('/login/concession-stand', catchErrors(appController.createShift));
router.get('/shift/progress', catchErrors(appController.progressPage));
router.post('/students/add', catchErrors(appController.addStudent));

// Calendar Routes
router.get(['/calendar','/'], calendarController.calendarPage);
router.get('/calendar/:id', calendarController.eventDetail);
router.get('/gcalendar', calendarController.gcalendarPage);

// Admin Routes
router.get('/admin/students', userController.isLoggedIn, userController.isAdmin, catchErrors(adminController.adminStudents));
router.get('/adminVariables', userController.isLoggedIn, catchErrors(adminController.adminVariables));
router.get('/admin/shifts', userController.isLoggedIn, userController.isManager, catchErrors(adminController.adminShifts));
router.get('/admin/users', userController.isLoggedIn, userController.isAdmin, catchErrors(adminController.adminUsers));
// router.get('/admin/users', catchErrors(adminController.adminUsers));
router.get('/admin/calendar', userController.isLoggedIn, catchErrors(adminController.adminCalendar));
router.get('/admin/calendar/:id', userController.isLoggedIn, catchErrors(adminController.eventDetail));

// User Login
router.get(['/admin/login','/admin'], userController.adminLogin);
router.post('/admin/login', userController.login);
// router.get('/admin/register', userController.adminRegister);
router.post('/admin/register', 
	userController.validateRegister,
	catchErrors(userController.register)
);
router.get('/admin/logout', userController.logout);
router.post('/admin/login', userController.login);


// API
// -- App
router.get('/api/shift/active', catchErrors(appController.loadActiveShifts));
router.get('/api/shift/:id', catchErrors(appController.getShift));
router.put('/api/shift/:id/end', catchErrors(appController.endShift));
router.put('/api/shift/:id/clear', catchErrors(appController.clearShift));
router.put('/api/student/addEarnings', catchErrors(appController.addEarnings));
router.post('/api/shift/email', catchErrors(appController.emailShift));
router.post('/api/email/shift-notification', catchErrors(appController.shiftNotification));

// -- Calendar
router.get('/api/calendar/getEvents', catchErrors(calendarController.getEvents));
router.post('/api/calendar/:id/attend', catchErrors(calendarController.registerAttendee));
router.get('/api/calendar/:id/:attendeeID/cancel', catchErrors(calendarController.cancelRSVP));

// -- Admin
router.post('/api/shift/add', catchErrors(adminController.addShift));
router.delete('/api/shift/clear', catchErrors(adminController.clearShifts));
router.delete('/api/shift/:id', catchErrors(adminController.deleteShift));
router.get('/admin/api/shift/:id', catchErrors(adminController.getShift));
router.post('/admin/api/shift/edit', catchErrors(adminController.editShift));
router.post('/api/student/add', catchErrors(adminController.addStudent));
router.delete('/api/student/:id/delete', catchErrors(adminController.deleteStudent));
router.put('/api/student/edit', catchErrors(adminController.editStudent));
router.put('/api/student/reset', catchErrors(adminController.resetEarnings));
router.put('/api/student/reset-all', catchErrors(adminController.resetAllEarnings));
router.delete('/api/user/:id/delete', catchErrors(adminController.deleteUser));
router.post('/api/addVariables', catchErrors(adminController.addVariables));
router.post('/api/calendar/add', catchErrors(adminController.addEvent));
router.get('/api/calendar/get/:id', catchErrors(adminController.getOneEvent));
router.delete('/api/calendar/delete/:id', catchErrors(adminController.deleteEvent));
router.post('/api/calendar/edit/:id', catchErrors(adminController.editEvent));
router.get('/api/calendar/:id/:attendeeID/removeVolunteer', catchErrors(adminController.removeVolunteer));
router.get('/api/downloads/:model', catchErrors(adminController.exportCSV));

// -- Import
router.post('/api/load', adminController.studentImportScript);

module.exports = router;