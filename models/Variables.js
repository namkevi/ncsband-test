const mongoose = require('mongoose');
const Schema = mongoose.Schema;
mongoose.Promise = global.Promise;

const variablesSchema = new Schema({
	wage: {
		type: Number,
		default: 9.00
	},
	max_hours: Number,
	locations: {
		type: Array,
		default: ['Big-Stand','Small-Stand']
	}
})

module.exports = mongoose.model('Variables', variablesSchema);