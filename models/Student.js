const mongoose = require('mongoose');
const Schema = mongoose.Schema;
mongoose.Promise = global.Promise;

const studentSchema = new Schema({
	name: {
		type: String,
		trim: true,
		required: 'Please enter a name',
		unique: true
	},
	hours: Number,
	reset_date: {
		type: Date,
		default: new Date()
	}
}, {
	toJSON: {virtuals: true},
	toObject: {virtuals: true}
});

studentSchema.virtual('shifts', {
	ref: 'Shift',
	localField: '_id',
	foreignField: 'student'
})

module.exports = mongoose.model('Student', studentSchema);