const mongoose = require('mongoose');
const Schema = mongoose.Schema;
mongoose.Promise = global.Promise;

const shiftSchema = new Schema ({
	startTime: String,
	endTime: String,
	duration: Number,
	name: {
		type: 'String',
		required: 'Please enter your name'
	},
	student: {
		type: mongoose.Schema.ObjectId,
		ref: 'Student',
		required: 'Student required'
	},
	location: String,
	notes: String,
	cleared: Boolean
});

shiftSchema.virtual('student_detail', {
	ref: 'Student',
	localField: 'student',
	foreignField: '_id'
})

module.exports = mongoose.model('Shift', shiftSchema);