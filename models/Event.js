const mongoose = require('mongoose');
const Schema = mongoose.Schema;
mongoose.Promise = global.Promise;

const attendeeSchema = new Schema ({
	name: {
		type: String,
		required: "Name required"
	},
	email: {
		type: String,
		required: "Email required",
		unique: true,
		dropDups: true
	},
	note: String
});

const eventSchema = new Schema ({
	start: {
		type: String,
		required: "Event start date required"
	},
	end: String,
	title: {
		type: String,
		required: "Event title required"
	},
	description: String,
	availableSlots: {
		type: Number,
		default: 0
	},
	filledSlots: {
		type: Number,
		default: 0
	},
	category: {
		type: String,
		required: "Event category required"
	},
	volunteerInformation: String,
	attendees: [attendeeSchema]
});

module.exports = mongoose.model('Event', eventSchema);