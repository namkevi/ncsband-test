const mail = require('@sendgrid/mail');
const pug = require('pug');
const juice = require('juice');
const htmlToText = require('html-to-text');
const promisify = require('es6-promisify');

mail.setApiKey(process.env.MAIL_KEY);

const generateHTML = (filename, options = {}) => {
	const html = pug.renderFile(`${__dirname}/../views/email/${filename}.pug`, options);
	const inlined = juice(html);
	return inlined;
}
exports.send = async(options) => {
	const html = generateHTML(options.filename, options);
	const text = htmlToText.fromString(html);
	const mailOptions = {
		from: options.from,
		to: options.to,
		subject: options.subject,
		html,
		text
	}
	const sendMail = promisify(mail.send, mail);
	return sendMail(mailOptions);
}